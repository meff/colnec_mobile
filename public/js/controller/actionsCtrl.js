'use strict';

function actionsCtrl($scope, $state, $window) {
    var TYPE_QUESTION = 1;
    var TYPE_BLOODPRESSURE = 2;

    $scope.items = [];

    {
        var rowItem = {};
        rowItem.title = "Mesurer tension artérielle";
        rowItem.icTitle = "ion-android-checkmark-circle";
        rowItem.type = TYPE_BLOODPRESSURE;

        $scope.items.push(rowItem);
    }

    {
        var rowItem = {};
        rowItem.title = "Préparer formulaire visite du...";
        rowItem.icTitle = "ion-android-checkmark-circle";
        rowItem.type = TYPE_QUESTION;

        $scope.items.push(rowItem);
    }


    $scope.onRowClicked = function(item, at) {
        switch (item.type) {
            case TYPE_QUESTION:
                $window.location.href = '#/tab/actionsQuestionToAsk';
                break;
            case TYPE_BLOODPRESSURE:
                $window.location.href = '#/tab/actionsMesurerBloodPressure';
                break;
            default:

        }

        //$window.location.href = '../../html/signIn.html';
    }
}


colnecMobile.controller('actionsCtrl', actionsCtrl);
