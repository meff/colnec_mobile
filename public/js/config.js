'use strict';

function config($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('tabs', {
            url: "/tab",
            abstract: true,
            templateUrl: "tabs.html"
        })
        .state('tabs.actions', {
            url: "/actions",
            views: {
                'actions-tab': {
                    templateUrl: "actions.html",
                    controller:"actionsCtrl"
                }
            }
        })
        .state('tabs.actionsQuestionToAsk', {
            url: "/actionsQuestionToAsk",
            views: {
                'actions-tab': {
                    templateUrl: "actionsQuestionToAsk.html",
                    controller:"actionsQuestionToAskCtrl"
                }
            }
        })
        .state('tabs.actionsMesurerBloodPressure', {
            url: "/actionsMesurerBloodPressure",
            views: {
                'actions-tab': {
                    templateUrl: "actionsMesurerBloodPressure.html",
                    controller:"actionsMesurerBloodPressureCtrl"
                }
            }
        })
        .state('tabs.journal', {
            url: "/journal",
            views: {
                'journal-tab': {
                    templateUrl: "journal.html",
                    controller:"journalCtrl"
                }
            }
        })
        .state('tabs.journalAddEdit', {
            url: "/journalAddEdit",
            views: {
                'journal-tab': {
                    templateUrl: "journalAddEdit.html",
                    controller:"journalAddEditCtrl"
                }
            }
        })
        .state('tabs.messages', {
            url: "/messages",
            views: {
                'messages-tab': {
                    templateUrl: "messages.html",
                    controller:"messagesCtrl"
                }
            }
        })
        .state('tabs.messagesAddEdit', {
            url: "/messagesAddEdit",
            views: {
                'messages-tab': {
                    templateUrl: "messagesAddEdit.html",
                    controller:"messagesAddEditCtrl"
                }
            }
        })
        .state('tabs.dashboard', {
            url: "/dashboard",
            views: {
                'dashboard-tab': {
                    templateUrl: "dashboard.html",
                    controller:"dashboardCtrl"
                }
            }
        });

    $urlRouterProvider.otherwise('/tab/actions');
}


colnecMobile
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
