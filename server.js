
'use strict';

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.redirect('/html/index.html');
});

app.listen(5555, function () {
    console.log('COLNEC Mobile run on port 5555!');
});
